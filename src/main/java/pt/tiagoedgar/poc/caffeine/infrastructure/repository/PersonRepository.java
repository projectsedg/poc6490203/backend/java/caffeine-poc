package pt.tiagoedgar.poc.caffeine.infrastructure.repository;

import pt.tiagoedgar.poc.caffeine.application.PersonService;
import pt.tiagoedgar.poc.caffeine.model.Person;
import java.util.List;

public class PersonRepository implements PersonService {

    private List<Person> persons;

    public PersonRepository(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public Person readByName(String name) {

        Person person = persons
                .stream()
                .filter(element -> name.equals(element.getName()))
                .findFirst()
                .orElse(null);

        System.out.println("-----> Repository Person ("+name+"): " + person);

        return person;
    }

    @Override
    public List<Person> readAll() {
        return persons;
    }
}
