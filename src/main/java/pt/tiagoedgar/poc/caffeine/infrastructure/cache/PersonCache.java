package pt.tiagoedgar.poc.caffeine.infrastructure.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import pt.tiagoedgar.poc.caffeine.application.PersonService;
import pt.tiagoedgar.poc.caffeine.model.Person;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PersonCache implements PersonService {

    private final LoadingCache<String, Person> cache;

    public PersonCache(PersonService personServiceRepository, int maxSize, boolean initialPopulate) {
        this.cache = Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .maximumSize(maxSize)
                .build(name -> personServiceRepository.readByName(name));

        if(initialPopulate){
            cache.putAll(personServiceRepository.readAll().stream()
                    .collect(Collectors.toMap(Person::getName, person -> person)));
        }

    }

    @Override
    public Person readByName(String name) {
        System.out.println("------------------ "+name+" -----------------");
        Person person = cache.get(name);
        System.out.println("Cache Person ("+name+"): " + person);
        return person;
    }

    @Override
    public List<Person> readAll() {
        return cache.asMap().values().stream().collect(Collectors.toList());
    }
}
